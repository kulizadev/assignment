### Kuliza News App

The goal of this project is to assess iOS development skills. It is designed to be challenging, and evaluate all aspects of development.

- **Project Name:** News App
- **Project Goal:** Creating a simplified version of the news app with limited functionality
- **Technology:** Objective-C or Swift 
- **Deliverables:** Build a news app. We are not concerned about theming but layouts should be proper. 

**Mockups:** 
- http://postimg.org/image/xa5vvt6vf/
- http://postimg.org/image/h027kui43/

**Description:**
* On top of our app, we will be flashing breaking News.The breaking News will automatically switch to next one after every 1.5 seconds. The breaking news part will stay fixed on top of screen.
* Below it, we will show news articles. The Layout of news articles is composed of one headline followed by 3 news article each in individual line and then 2 news item in one line (refer the mockups for better clarity).  
* The news article will contain news image and news desc. News desc could be large. Whole news desc should be visible on screen. Increase the height of news article if needed for this.
* On click of any news article, you have to open the news in webview by picking the article link from ***article_url*** property
* It will be appreciated that logic for determining as which news article will occupy whole screen and which one will occupy half part of screen is not fixed for 3 and 2 but rather is derived on basis of x & y in the project.
* **Ignore the bottom tabbar part and search bar**


**API to consume:** http://pastebin.com/ is our production RESTful JSON API (The endpoint is https friendly too)

**List of News Items** - http://pastebin.com/raw/EUxVs4Ns - HomePage

- **message.main_story** - to be used for headline news article
- **message.latest_news** - array of news article, first 3 will cover whole screen width and then next 2 will cover half of the screen. then next 3 will again cover whole screen and then next 2 half of screen and so on..
- **message.breaking_news** - array of articles titles which will flash as breaking news on top of screen.


**Guidelines**

The application has to be runnable and working without major issues.

Ask any questions related to the implementation, but stackoverflow and google are your first stop for any obvious questions.

**What is this?**

This repo contains the job assignment for potential iOS developers at Kuliza Technologies Pvt. Ltd.